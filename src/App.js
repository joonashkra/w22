
import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>W22 Hello country example</p>
      </header>
      <ComponentName country="Finland"/>
    </div>
  );
}

export default App;
